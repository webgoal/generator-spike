gem 'simple_form'
gem 'atelie-layout', git: 'https://bitbucket.org/webgoal/atelie-layout.git'
gem 'kaminari'
gem 'paranoia'
gem 'audited'
gem 'yui-compressor' # Use Yui-compressor as compressor for CSS assets
gem 'sass-rails' # Use SCSS for stylesheets
gem 'bootstrap', '~> 4.0.0.alpha6'
gem 'bootstrap-datepicker-rails'

#####################################################################
# Executar o Bundle e Install antes de habilitar os Mysql ou Postgree
run 'bundle install'
run "rails generate simple_form:install"
run "rails generate atelie_layout:install"
#####################################################################

%w(sqlite3 jbuilder).each do |unwanted_gem|
  gsub_file("Gemfile", /gem '#{unwanted_gem}'.*\n/, '')
end

# Definindo Database
database_type = ask("Do you want to use postgres or mysql?", limited_to: ["pg", "mysql"])

if database_type == 'pg'
  gem 'pg'
  adapter = 'postgresql'
  db = <<-FILE
    image: postgres:9.4.9
    environment:
      POSTGRES_USER: user
      POSTGRES_DB: <%= @app_name %>_development
    ports:
      - "5432:5432"
FILE
else
  gem 'mysql2'
  adapter = 'mysql2'
  db = <<-FILE
    image: mysql:5.6
    ports:
      - 3360:3306
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
      MYSQL_USER: root
      MYSQL_DATABASE: <%= @app_name %>_development
FILE
end

database_url = "#{adapter}://root@dbhost/"

# Docker-Compose
dockercompose_file = <<-FILE
version: '2'

volumes:
  gems:

services:
  web:
    build:
      dockerfile: dockerfiles/Dockerfile.development
      context: .
    environment:
      TZ: 'America/Sao_Paulo'
      DATABASE_URL: "<%= database_url %>"
    links:
      - db:dbhost
    ports:
      - "3000:3000"
    volumes:
      - gems:/usr/local/bundle
      - ./:/usr/src/app
    command: rails s -p 3000 -b '0.0.0.0'

  db:
<%= db %>
FILE

create_file 'docker-compose.yml', ERB.new(dockercompose_file).result(binding), force: true

# Dockerfile
dockerfile_file = <<-FILE
FROM webgoal/generic_ror

WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 3000
FILE

create_file 'dockerfiles/Dockerfile.development', ERB.new(dockerfile_file).result(binding), force: true

environment 'config.generators.jbuilder = false'

run "export DATABASE_URL=#{database_url}"

database_file = <<-FILE
default: &default
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  timeout: 5000
development:
  <<: *default
  database: <%= @app_name %>_development
  password:
test:
  <<: *default
  database: <%= @app_name %>_test
production:
  <<: *default
  database: <%= @app_name %>_production
FILE

create_file 'config/database.yml', ERB.new(database_file).result(binding), force: true
